exports.getmessage = function (update, bot) {
	// to resolve circular dependency
	const {
		Message
	} = require('./types');

	const message = update.message || update.edited_message;

	if (message) {
		return new Message(message, bot)
	}
}

exports.getuser = function (from, bot) {
	const { users } = bot;

	for (const index in users) {
		if (users[index].id === from.id) {
			return bot.users[index];
		}
	}

	bot.users.push(from);

	return bot.users[bot.users.length-1];
}

/**
 * Match type with message type
 * @param {String | Array} type
 * @param {String} msgType
 * @return {Boolean}
 */
exports.matchType = function (type, msgType) {
	if (Array.isArray(type)) {
		const types = type;

		for (type of types) {
			if (matchType(type, msgType)) {
				return true;
			}
		}
	}

	if (typeof type === 'string') {
		return type === msgType;
	}

	return false;
}
<div align="center"> <h1> Telegram bot API wrapper for LibTelegram </h1> </div>

This is Telegram bot API wrapper for LibTelegram that give you full Telegram bot API support.

Features:
- Telegram bot API 4.9
- 40+ telegram objects represent like classes
- Polling
- Webhooks
- User message captures & scene path

## Usage example
```js
const Telegram = require('telegram');

class Library extends Telegram {
    constructor (token) {
        super(token);
    }

    handle (usr, msg) {
        console.log(usr, msg)
    }
}

const bot = new Library(process.env.TOKEN);

bot.startPolling();
```

## Wiki

// no info

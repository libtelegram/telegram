const request = require('request');

const {
	User, 
	Message,
	Chat,
	BotCommand
} = require('./types');

const { 
	getmessage,
	getuser
} = require('./utils');

module.exports = class Telegram {
	constructor (token) {
		this._token = token;
	};

	startPolling (interval = 300, extra = {}) {
		this.polling = Object.assign(
			{}, this.polling, extra, { interval, started: true }
		);
	
		this.fetchUpdates();
	}
	
	stopPolling () {
		this.polling.started = false;
	}
	
	async fetchUpdates () {
		const { interval, started } = this.polling;
	
		if (started) {
			const updates = await this.getUpdates(this.polling);
	
			for (const update of updates) {
				const msg = getmessage(update, this),
				      usr = getuser(msg.from, this);

				this.handle(usr, msg);
			}
	
			if (updates.length) {
				this.polling.offset = updates[updates.length - 1].update_id + 1;
			}
		
			this.timer = setTimeout(() => this.fetchUpdates(), interval);
		}
	}

	/** 
	 * A simple method for testing your bot's auth token. 
	 * Requires no parameters. 
	 * Returns basic information about the bot in form of a User class.
	 */
	async getMe () {
		const res = await fetch(this._token, 'getMe');

		return new User(res, this);
	};

	/**
	 * Use this method to receive incoming updates using long polling (wiki). 
	 * An Array of Update objects is returned.
	 */
	async getUpdates (extra) {
		return await fetch(this._token, 'getUpdates', extra);
	};

	async deleteWebhook () {
		return await fetch(this._token, 'deleteWebhook');
	}

	async setWebhook (url, extra) {
		return await fetch(this._token, 'setWebhook', { url, ...extra });
	}

	async getWebhookInfo () {
		return await fetch(this._token, 'getWebhookInfo');
	}

	async sendMessage (chat_id, text, extra) {
		const res = await fetch(this._token, 'sendMessage', { chat_id, text, ...extra });

		return new Message(res, this);
	}

	async forwardMessage (chat_id, from_chat_id, message_id, disable_notification) {
		const res = await fetch(this._token, 'forwardMessage', { 
			chat_id, 
			from_chat_id, 
			message_id, 
			disable_notification 
		});

		return new Message(res, this);
	}

	async sendPhoto (chat_id, photo, extra) {
		const res = await fetch(this._token, 'sendPhoto', { 
			chat_id, 
			photo, 
			caption, 
			...extra 
		});

		return new Message(res, this);
	}

	async sendAudio (chat_id, audio, extra) {
		const res = await fetch(this._token, 'sendAudio', { chat_id, audio, ...extra });

		return new Message(res, this);
	}

	async sendDocument (chat_id, document, extra) {
		const res = await fetch(this._token, 'sendDocument', { chat_id, document, ...extra });

		return new Message(res, this);
	}

	async sendVideo (chat_id, video, extra) {
		const res = await fetch(this._token, 'sendVideo', { chat_id, video, ...extra });

		return new Message(res, this);
	}

	async sendAnimation (chat_id, animation, extra) {
		const res = await fetch(this._token, 'sendAnimation', { chat_id, animation, ...extra });

		return new Message(res, this);
	}

	async sendVoice (chat_id, voice, extra) {
		const res = await fetch(this._token, 'sendVoice', { chat_id, voice, ...extra });

		return new Message(res, this);
	}

	async sendVideoNote (chat_id, video_note, extra) {
		const res = await fetch(this._token, 'sendVideoNote', { chat_id, video_note, ...extra});

		return new Message(res, this);
	}

	async sendMediaGroup (chat_id, media, extra) {
		const res = await fetch(this._token, 'sendMediaGroup', { chat_id, media, ...extra});

		return new Message(res, this);
	}

	async sendLocation (chat_id, latitude, longitude, extra) {
		const res = await fetch(this._token, 'sendLocation', { chat_id , latitude, longitude, ...extra});

		return new Message(res, this);
	}

	async sendVenue (chat_id, latitude, longitude, address, title , extra) {
		const res = await fetch(this._token, 'sendVenue', { 
			chat_id, 
			latitude, 
			longitude, 
			title, 
			address, 
			...extra
		});

		return new Message(res, this);
	}

	async sendContact (chat_id, phone_number, first_name, extra) {
		const res = await fetch(this._token, 'sendContact', { 
			chat_id, 
			phone_number, 
			first_name, 
			...extra
		});

		return new Message(res, this);
	}

	async sendPoll (chat_id, question, options, extra) {
		const res = await fetch(this._token, 'sendPoll', { 
			chat_id, 
			question, 
			options,
			...extra 
		});

		return new Message(res, this);
	}

	async sendChatAction (chat_id, action) {
		const res = await fetch(this._token, 'sendChatAction', { chat_id, action });

		return new Message(res, this);
	}

	async sendDice (chat_id, emoji, extra) {
		const res = await fetch(this._token, 'sendDice', { emoji, ...extra });

		return new Message(res, this);
	}

	async setMyCommands (commands) {
		const res = await fetch(this._token, 'sendDice', { commands });

		return res;
	}

	async getMyCommands () {
		const res = await fetch(this._token, 'getMyCommands');

		return res.map(command => new BotCommand(command));
	}

	async getUserProfilePhotos (user_id, offset, limit) {
		return await fetch(this._token, 'getUserProfilePhotos', { user_id, offset, limit });
	}

	async getFile (file_id) {
		return await fetch(this._token, 'getFile', { file_id });
	}

	async kickChatMember (chat_id, user_id, until_date) {
		return await fetch(this._token, 'kickChatMember', { chat_id, user_id, until_date });
	}

	async unbanChatMember (chat_id, user_id) {
		return await fetch(this._token, 'unbanChatMember', { chat_id, user_id });
	}

	async restrictChatMember (chat_id, user_id, permissions, until_date) {
		return await fetch(this._token, 'restrictChatMember', { chat_id, user_id, permissions, until_date });
	}

	async promoteChatMember (chat_id, user_id, extra) {
		return await fetch(this._token, 'promoteChatMember', { chat_id, user_id, ...extra });
	}

	async setChatAdministratorCustomTitle (chat_id, user_id, custom_title) {
		return await fetch(this._token, 'setChatAdministratorCustomTitle', { chat_id, user_id, custom_title });
	}

	async setChatPermissions (chat_id, permissions) {
		return await fetch(this._token, 'setChatPermissions', { chat_id, permissions });
	}

	async setChatPhoto (chat_id, photo) {
		return await fetch(this._token, 'setChatPhoto', { chat_id, photo });
	}

	async deleteChatPhoto (chat_id) {
		return await fetch(this._token, 'deleteChatPhoto', { chat_id });
	}

	async setChatTitle (chat_id, title) {
		return await fetch(this._token, 'setChatTitle', { chat_id, title });
	}

	async setChatDescription (chat_id, user_id, custom_title) {
		return await fetch(this._token, 'setChatDescription', { chat_id, user_id, custom_title });
	}

	async pinChatMessage (chat_id, message_id, disable_notification) {
		return await fetch(this._token, 'pinChatMessage', { chat_id, message_id, disable_notification });
	}

	async unpinChatMessage (chat_id) {
		return await fetch(this._token, 'unpinChatMessage', { chat_id });
	}

	async leaveChat (chat_id) {
		return await fetch(this._token, 'leaveChat', { chat_id });
	}

	async getChat (chat_id) {
		const res = await fetch(this._token, 'getChat', { chat_id });

		return new Chat(res, this);
	}

	async getChatAdministrators (chat_id) {
		return await fetch(this._token, 'getChatAdministrators', { chat_id });
	}

	async getChatMembersCount (chat_id) {
		return await fetch(this._token, 'getChatMembersCount', { chat_id });
	}

	async getChatMember (chat_id, user_id) {
		return await fetch(this._token, 'getChatMember', { chat_id, user_id });
	}

	async setChatStickerSet (chat_id, sticker_set_name) {
		return await fetch(this._token, 'setChatStickerSet', { chat_id, sticker_set_name });
	}

	async deleteChatStickerSet (chat_id) {
		return await fetch(this._token, 'deleteChatStickerSet', { chat_id });
	}

	async answerCallbackQuery (callback_query_id, extra) {
		return await fetch(this._token, 'answerCallbackQuery', { callback_query_id, ...extra });
	}

	async exportChatInviteLink (chat_id) {
		return await fetch(this._token, 'exportChatInviteLink', { chat_id });
	}

	async editMessageText (chat_id, message_id, text, extra) {
		return await fetch(this._token, 'editMessageText', { chat_id, message_id, text, ...extra });
	}

	async answerInlineQuery (inline_query_id, results, extra){
		return await fetch(this._token, 'answerInlineQuery', { inline_query_id, results, ...extra });
	}

}

/**
 * Fething the request to telegram bot api
 * @param {string} token 
 * @param {string} methodName 
 * @param {object} params 
 */
async function fetch (token, methodName, params) {
	
	return new Promise ((resolve, reject) => {
		const URL = `https://api.telegram.org/bot${token}/${methodName}`;

		request.post(URL, { form: params }, function (error, response, body) {
			if (error) reject(error);

			body = JSON.parse(body);

			if (!body.ok) {
				return reject(
					`an error occurred while processing ${methodName}: ${body.error_code} ${body.description}`
				);
			}

			resolve(body.result);
		});
	});
	
}
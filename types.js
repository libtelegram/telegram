const path = require('path');

const { matchType } = require('./utils');

/**
 * This class represents a chat.
 */
class Chat {
	constructor(params = {}, bot) {
		this.telegram = bot;

		this.id = params.id;
		this.type = params.type;
		this.title = params.title;
		this.username = params.username;
		this.first_name = params.first_name;
		this.last_name = params.last_name;
		this.photo = params.photo ? new ChatPhoto(params.photo) : undefined;
		this.description = params.description;
		this.invite_link = params.invite_link;
		this.pinned_message = params.pinned_message ? new Message(params.pinned_message) : undefined;
		this.permissions = params.permissions ? new ChatPermissions(params.permissions) : undefined;
		this.slow_mode_delay = params.slow_mode_delay;
		this.sticker_set_name = params.sticker_set_name;
		this.can_set_sticker_set = params.can_set_sticker_set;
	}

	setPermissions (permissions) {
		return this.telegram.setChatPermissions(this.id || this.username, permissions);
	}

	exportInviteLink () {
		return this.telegram.exportChatInviteLink(this.id || this.username);
	}

	setPhoto (photo) {
		return this.telegram.setChatPhoto(this.id || this.username, photo);
	}

	deletePhoto () {
		return this.telegram.deleteChatPhoto(this.id || this.username);
	}

	setTitle (title) {
		return this.telegram.setChatTitle(this.id || this.username, title);
	}

	setDescription (description) {
		return this.telegram.setChatDescription(this.id || this.username, description);
	}

	pinMessage (message_id, disable_notification) {
		return this.telegram.pinChatMessage(this.id || this.username, message_id, disable_notification);
	}

	unpinMessage () {
		return this.telegram.unpinChatMessage(this.id || this.username);
	}

	leave () {
		return this.telegram.leaveChat(this.id || this.username);
	}

	get () {
		return this.telegram.getChat(this.id || this.username);
	}

	getAdministrators () {
		return this.telegram.getChatAdministrators(this.id || this.username);
	}

	getMembersCount () {
		return this.telegram.getChatMembersCount(this.id || this.username);
	}

	getMember (user_id) {
		return this.telegram.getChatMember(this.id || this.username, user_id);
	}

	setStickerSet (sticker_set_name) {
		return this.telegram.setChatStickerSet(this.id || this.username, sticker_set_name);
	}

	deleteStickerSet () {
		return this.telegram.deleteChatStickerSet(this.id || this.username);
	}

	kick (user_id, until_date) {
		return this.telegram.kickChatMember(this.id || this.username, user_id, until_date);
	}

	unban (user_id) {
		return this.telegram.unbanChatMember(this.id || this.username, user_id);
	}

	restrict (user_id, permissions, until_date) {
		return this.telegram.restrictChatMember(user_id, permissions, until_date);
	}

	promote (user_id, opts) {
		return this.telegram.promoteChatMember(this.id || this.username, user_id, opts);
	}

	setAdministratorCustomTitle (user_id, custom_title) {
		return this.telegram.setChatAdministratorCustomTitle(this.id || this.username, user_id, custom_title);
	}
}

class ShippingQuery {
	constructor(params = {}, bot) {
		this.id = params.id;
		this.from = params.from ? new User(params.from) : null;
		this.invoice_payload = params.invoice_payload;
		this.shipping_address = params.shipping_address ? new ShippingAddress(params.shipping_address) : null;
	}
}

class PreCheckoutQuery {
	constructor (params = {}, bot) {
		this.id = params.id;
		this.from = new User(params.from);
		this.currency = params.currency;
		this.total_amount = params.total_amount;
		this.invoice_payload = params.invoice_payload;
		this.shipping_option_id = params.shipping_option_id;
		this.order_info = params.order_info? new OrderInfo(params.order_info): undefined;
	}
}

class Poll {
	constructor(params = {}, bot) {
		this.id = params.id;
		this.question = params.question;
		this.options = params.options ? params.options.map(option => new PollOption(option)) : undefined;
		this.total_voter_count = params.total_voter_count;
		this.is_closed = params.is_closed;
		this.is_anonymous = params.is_anonymous;
		this.type = params.type;
		this.allows_multiple_answers = params.allows_multiple_answers;
		this.correct_option_id = params.correct_option_id;
		this.open_period = params.open_period;
		this.close_date = params.close_date;
		this.explanation = params.explanation;
		this.explanation_entities = params.explanation_entities? params.explanation_entities.map(entity => new MessageEntity(entity)): undefined;
	}
}

class PollAnswer {
	constructor (params = {}, bot) {
		this.poll_id = opts.poll_id;
		this.user = new User(opts.user, bot);
		this.option_ids = opts.option_ids;
	}
}


class InlineQuery {
	constructor(params = {}, bot) {
		this.id = params.id;
		this.from = params.from ? new User(params.from) : null;
		this.location = params.location ? new Location(params.location) : null;
		this.query = params.query;
		this.offset = params.offset;
	}
}

class ChosenInlineResult {
	constructor(params = {}, bot) {
		this.result_id = params.result_id;
		this.from = params.from ? new User(params.from) : null;
		this.location = params.location ? new Location(params.location) : null;
		this.inline_message_id = params.inline_message_id;
		this.query = params.query;
	}
}


class CallbackQuery {
	constructor(params = {}, bot) {
		this.id = params.id;
		this.from = params.from ? new User(params.from) : null;
		this.message = params.message ? new Message(params.message) : null;
		this.inline_message_id = params.inline_message_id;
		this.chat_instance = params.chat_instance;
		this.data = params.data;
		this.game_short_name = params.game_short_name;
	}
}


/**
 * This class represents a message.
 */
class Message {
	constructor (params = {}, bot) {
		this.telegram = bot;

		this.type = 'text';

		this.message_id = params.message_id;

		this.from = params.from? new User(params.from, bot) : undefined;

		this.date = params.date;
		this.chat = new Chat(params.chat, bot);
		this.forward_from = params.forward_from? new User(params.forward_from, bot) : undefined;
		this.forward_from_chat = params.forward_from_chat;
		this.forward_from_message_id = params.forward_from_message_id;
		this.forward_signature = params.forward_signature;
		this.forward_sender_name = params.forward_sender_name;
		this.forward_date = params.forward_date;
		this.reply_to_message = params.reply_to_message? new Message(params.reply_to_message, bot) : undefined;
		this.edit_date = params.edit_date;
		this.media_group_id = params.media_group_id;
		this.author_signature = params.author_signature;
		this._text = params.text;
		this.entities = params.entities? params.entities.map(entity => new MessageEntity(entity, bot)) : undefined;
		this.caption_entities = params.caption_entities? params.caption_entities.map(entity => new MessageEntity(entity, bot)) : undefined;
		this.audio = params.audio? new Audio(params.audio, bot) : undefined;
		this.document = params.document? new Document(params.document, bot) : undefined;
		this.animation = params.animation? new Animation(params.animation, bot) : undefined;
		this.game = params.game? new Game(params.game, bot) : undefined;
		this.photo = params.photo? params.photo.map(photo => new PhotoSize(photo, bot)) : undefined;
		this.sticker = params.sticker? new Sticker(params.sticker, bot) : undefined;
		this.video = params.video? new Video(params.video, bot) : undefined;
		this.voice = params.voice? new Voice(params.voice, bot) : undefined;
		this.video_note = params.video_note? new VideoNote(params.video_note, bot) : undefined;
		this.caption = params.caption;
		this.contact = params.contact? new Contact(params.contact, bot) : undefined;
		this.location = params.location? new Location(params.location, bot) : undefined;
		this.venue = params.venue? new Venue(params.venue, bot) : undefined;
		this.poll = params.poll? new Poll(params.poll, bot) : undefined;
		this.new_chat_members = params.new_chat_members? params.new_chat_members.map(user => new User(user, bot)) : undefined;
		this.left_chat_member = params.left_chat_member? new User(params.left_chat_member, bot) : undefined;
		this.new_chat_title = params.new_chat_title;
		this.new_chat_photo = params.new_chat_photo? params.new_chat_photo(photo => new PhotoSize(photo, bot)) : undefined;
		this.delete_chat_photo = params.delete_chat_photo;
		this.group_chat_created = params.group_chat_created;
		this.supergroup_chat_created = params.supergroup_chat_created;
		this.channel_chat_created = params.channel_chat_created;
		this.migrate_to_chat_id = params.migrate_to_chat_id;
		this.migrate_from_chat_id = params.migrate_from_chat_id;
		this.pinned_message = params.pinned_message? new Message(params.pinned_message, bot) : undefined;
		this.invoice = params.invoice? new Invoice(params.invoice, bot) : undefined;
		this.successful_payment = params.successful_payment? new SuccessfulPayment(this.successful_payment, bot) : undefined;
		this.connected_website = params.connected_website;
		this.passport_data = params.passport_data? new PassportData(params.passport_data, bot) : undefined;
		this.reply_markup = params.reply_markup? new InlineKeyboardMarkup(params.reply_markup, bot) : undefined;
		this.dice = params.dice? new Dice(params.dice, bot): undefined;
	}

	get text () {
		return this._text;
	}

	set text (value) {
		this.telegram.editMessageText(this.chat.id, this.message_id, value);
	}

	async reply (text, opts) {
		const msg = this.telegram.sendMessage(this.chat.id, text, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithPhoto (photo, opts) {
		const msg = await this.telegram.sendPhoto(this.chat.id, photo, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithAudio (audio, opts) {
		const msg = await this.telegram.sendAudio(this.chat.id, audio, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithDocument (document, opts) {
		const msg = await this.telegram.sendDocument(this.chat.id, document, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithVideo (video, opts) {
		const msg = await this.telegram.sendVideo(this.chat.id, video, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithAnimation (animation, opts) {
		const msg = await this.telegram.sendAnimation(this.chat.id, animation, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithVoice (voice, opts) {
		const msg = await this.telegram.sendVoice(this.chat.id, voice, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithVideoNote (video_note, opts) {
		const msg = await this.telegram.sendVideoNote(this.chat.id, video_note, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithMediaGroup (media, opts) {
		const msg = await this.telegram.sendMediaGroup(this.chat.id, media, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithLocation (latitude, longitude, opts) {
		const msg = await this.telegram.sendLocation(this.chat.id, latitude, longitude, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithVenue (latitude, longitude, address, title, opts) {
		const msg = await this.telegram.sendVenue(this.chat.id, latitude, longitude, address, title, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithContact (phone_number, first_name, opts) {
		const msg = await this.telegram.sendContact(this.chat.id, phone_number, first_name, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithPoll (question, options, opts) {
		const msg = await this.telegram.sendPoll(this.chat.id, question, options, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	async replyWithChatAction (action, opts) {
		const msg = await this.telegram.sendChatAction(this.chat.id, action, {
			reply_to_message_id: this.message_id,
			...opts
		});

		return new Message(msg, this.telegram);
	}

	getUser () {
		return this.telegram.getUser(this.from);
	}
}


/**
 * This class represents a Telegram user or bot.
 */

class User {
	constructor (params = {}, bot) {
		this.telegram = bot;

		this.id = params.id;

		this.is_bot = params.is_bot;

		this.first_name = params.first_name;
		this.last_name = params.last_name;
		this.username = params.username;

		this.lang = this.language_code = params.language_code;

		this.can_join_groups = params.can_join_groups;
		this.can_read_all_group_messages = params.can_read_all_group_messages;

		this.supports_inline_queries = params.supports_inline_queries;

		this._state = {};
		this.path = '/';
		this.captures = [];
	}

	scene (scenePath) {
		if (typeof scenePath !== 'string' || !scenePath.length) {
			return;
		}

		this.telegram.leave(this.path, this);

		// root
		if (scenePath[0] === '/') {
			scenePath = scenePath.slice(1);

			this.path = path.normalize(scenePath);
		} 

		// local
		else {
			this.path = path.normalize(this.path + '/' + scenePath);
		}

		this.telegram.enter(this.path, this);
	}

	get isWaiting () {
		return this.captures.length !== 0;
	}

	get state () {
		return this._state;
	}

	set state (value) {
		this._state = { ...value };
	}

	handle (msg) {
		for (const index in this.captures) {
			const { type, timer, resolve } = this.captures[index];
	
			const callback = () => {
				resolve(msg);
		
				return this.captures.splice(+index, 1); 
			}
	
			if (capture.id === usr.id) {				
				if (matchType(type, msg.type)) {
					if (timer) {
						clearTimeout(timer);
					}

					return callback();
				}
	
				return;
			}
		}
	}

	wait (type, delay, callback) {
		const promise = new Promise ((resolve, reject) => {
			let timer;

			if (delay) {
				timer = setTimeout(() => {
					if (callback) {
						callback(this);
					}
				}, delay);
			}

			this.captures.push({ 
				type,
				resolve,
				timer,
				id: this.id, 
			});
		})
		
		return promise;
	}


	async reply (text, extra) {
		const msg = await this.telegram.sendMessage(this.id, text, extra);

		return new Message(msg, this.telegram);
	}

	async replyWithPhoto (photo, opts) {
		const msg = await this.telegram.sendPhoto(this.id, photo, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithAudio (audio, opts) {
		const msg = await this.telegram.sendAudio(this.id, audio, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithDocument (document, opts) {
		const msg = await this.telegram.sendDocument(this.id, document, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithVideo (video, opts) {
		const msg = await this.telegram.sendVideo(this.id, video, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithAnimation (animation, opts) {
		const msg = await this.telegram.sendAnimation(this.id, animation, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithVoice (voice, opts) {
		const msg = await this.telegram.sendVoice(this.id, voice, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithVideoNote (video_note, opts) {
		const msg = await this.telegram.sendVideoNote(this.id, video_note, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithMediaGroup (media, opts) {
		const msg = await this.telegram.sendMediaGroup(this.id, media, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithLocation (latitude, longitude, opts) {
		const msg = await this.telegram.sendLocation(this.id, latitude, longitude, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithVenue (latitude, longitude, address, title, opts) {
		const msg = await this.telegram.sendVenue(this.id, latitude, longitude, address, title, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithContact (phone_number, first_name, opts) {
		const msg = await this.telegram.sendContact(this.id, phone_number, first_name, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithPoll (question, options, opts) {
		const msg = await this.telegram.sendPoll(this.id, question, options, opts);

		return new Message(msg, this.telegram);
	}

	async replyWithChatAction (action, opts) {
		const msg = await this.telegram.sendChatAction(this.id, action, opts);

		return new Message(msg, this.telegram);
	}
}
class ChatPermissions {
	constructor(params = {}, bot) {
		this.can_send_messages = params.can_send_messages;
		this.can_send_media_messages = params.can_send_media_messages;
		this.can_send_polls = params.can_send_polls;
		this.can_send_other_messages = params.can_send_other_messages;
		this.can_add_web_page_previews = params.can_add_web_page_previews;
		this.can_change_info = params.can_change_info;
		this.can_invite_users = params.can_invite_users;
		this.can_pin_messages = params.can_pin_messages;
	}
}

class ChatPhoto {
	constructor(params = {}, bot) {
		this.small_file_id = params.small_file_id;
		this.small_file_unique_id = params.small_file_unique_id;
		this.big_file_id = params.big_file_id;
		this.big_file_unique_id = params.big_file_unique_id;
	}
}

class InlineKeyboardMarkup {
	constructor(params = {}, bot) {
		this.inline_keyboard = params.inline_keyboard ? params.inline_keyboard.map(keyboard => keyboard.map(button => InlineKeyboardButton(button, bot))) : undefined;
	}
}

class InlineKeyboardButton {
	constructor(params = {}, bot) {
		this.text = params.text;
		this.url = params.url;
		this.login_url = params.login_url ? new LoginUrl(params.login_url, bot) : undefined;
		this.callback_data = params.callback_data;
		this.switch_inline_query = params.switch_inline_query;
		this.switch_inline_query_current_chat = params.switch_inline_query_current_chat;
		this.callback_game = params.callback_game ? new CallbackGame(params.callback_game, bot) : undefined;
		this.pay = params.pay;
	}
}

class CallbackGame {
	constructor (params = {}, bot) {
		// A placeholder, currently holds no information. Use BotFather to set up your game.
	}
}

class LoginUrl {
	constructor(params = {}, bot) {
		this.url = params.url;
		this.forward_text = params.forward_text;
		this.bot_username = params.bot_username;
		this.request_write_access = params.request_write_access;
	}
}

class MessageEntity {
	constructor(params = {}, bot) {
		this.type = params.type;
		this.offset = params.offset;
		this.length = params.length;
		this.url = params.url;
		this.user = params.user ? new User(params.user, bot) : undefined;
		this.lang = this.language = params.language;
	}
}

class Audio {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.duration = params.duration;
		this.performer = params.performer;
		this.title = params.title;
		this.mime_type = params.mime_type;
		this.file_size = params.file_size;
		this.thumb = params.thumb ? new PhotoSize(params.thumb, bot) : undefined;
	}
}

class PhotoSize {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.width = params.width;
		this.height = params.height;
		this.file_size = params.file_size;
	}
}

class Document {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.thumb = params.thumb ? new PhotoSize(params.thumb, bot) : undefined;
		this.file_name = params.file_name;
		this.mime_type = params.mime_type;
		this.file_size = params.file_size;
	}
}

class Animation {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.width = params.width;
		this.height = params.height;
		this.duration = params.duration;
		this.thumb = params.thumb ? new PhotoSize(params.thumb, bot) : undefined;
		this.file_name = params.file_name;
		this.mime_type = params.mime_type;
		this.file_size = params.file_size;
	}
}

class Game {
	constructor(params = {}, bot) {
		this.title = params.title;
		this.description = params.description;
		this.photo = params.photo ? params.photo.map(photo => new PhotoSize(photo, bot)) : undefined;
		this.text = params.text;
		this.text_entities = params.text_entities ? params.text_entities.map(entity => new MessageEntity(entity, bot)) : undefined;
		this.animation = params.animation ? new Animation(params.animation, bot) : undefined;
	}
}

class Sticker {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.width = params.width;
		this.height = params.height;
		this.is_animated = params.is_animated;
		this.thumb = params.thumb ? new PhotoSize(params.thumb, bot) : undefined;
		this.emoji = params.emoji;
		this.set_name = params.set_name;
		this.mask_position = params.mask_position ? new MaskPosition(params.mask_position, bot) : undefined;
		this.file_size = params.file_size;
	}
}

class MaskPosition {
	constructor(params = {}, bot) {
		this.point = params.point;
		this.x_shift = params.x_shift;
		this.y_shift = params.y_shift;
		this.scale = params.scale;
	}
}

class Video {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.width = params.width;
		this.height = params.height;
		this.duration = params.duration;
		this.thumb = params.thumb ? new PhotoSize(params.thumb, bot) : undefined;
		this.mime_type = params.mime_type;
		this.file_size = params.file_size;
	}
}

class Voice {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.duration = params.duration;
		this.mime_type = params.mime_type;
		this.file_size = params.file_size;
	}
}

class VideoNote {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.length = params.length;
		this.duration = params.duration;
		this.thumb = params.thumb ? new PhotoSize(params.thumb, bot) : undefined;
		this.file_size = params.file_size;
	}
}

class Contact {
	constructor(params = {}, bot) {
		this.phone_number = params.phone_number;
		this.first_name = params.first_name;
		this.last_name = params.last_name;
		this.user_id = params.user_id;
		this.vcard = params.vcard;
	}
}

class Location {
	constructor(params = {}, bot) {
		this.longitude = params.longitude;
		this.latitude = params.latitude;
	}
}

class Venue {
	constructor(params = {}, bot) {
		this.location = params.location ? new Location(params.location) : undefined;
		this.title = params.title;
		this.address = params.address;
		this.foursquare_id = params.foursquare_id;
		this.foursquare_type = params.foursquare_type;
	}
}

class PollOption {
	constructor(params = {}, bot) {
		this.text = params.text;
		this.voter_count = params.voter_count;
	}
}

class Invoice {
	constructor(params = {}, bot) {
		this.title = params.title;
		this.description = params.description;
		this.start_parameter = params.start_parameter;
		this.currency = params.currency;
		this.total_amount = params.total_amount;
	}
}

class SuccessfulPayment {
	constructor(params = {}, bot) {
		this.currency = params.currency;
		this.total_amount = params.total_amount;
		this.invoice_payload = params.invoice_payload;
		this.shipping_option_id = params.shipping_option_id;
		this.order_info = params.order_info ? new OrderInfo(params.order_info, bot) : undefined;
		this.telegram_payment_charge_id = params.telegram_payment_charge_id;
		this.provider_payment_charge_id = params.provider_payment_charge_id;
	}
}

class OrderInfo {
	constructor(params = {}, bot) {
		this.name = params.name;
		this.phone_number = params.phone_number;
		this.email = params.email;
		this.shipping_address = params.shipping_address ? new ShippingAddress(params.shipping_address, bot) : undefined;
	}
}

class ShippingAddress {
	constructor(params = {}, bot) {
		this.country_code = params.country_code;
		this.state = params.state;
		this.city = params.city;
		this.street_line1 = params.street_line1;
		this.street_line2 = params.street_line2;
		this.post_code = params.post_code;
	}
}

class PassportData {
	constructor(params = {}, bot) {
		this.data = params.data ? params.data.map(data => new EncryptedPassportElement(data, bot)) : undefined;
		this.credentials = params.credentials ? new EncryptedCredentials(params.credentials, bot) : undefined;
	}
}

class EncryptedCredentials {
	constructor(params = {}, bot) {
		this.data = params.data;
		this.hash = params.hash;
		this.secret = params.secret;
	}
}

class EncryptedPassportElement {
	constructor(params = {}, bot) {
		this.type = params.type;
		this.data = params.data;
		this.phone_number = params.phone_number;
		this.email = params.email;
		this.files = params.files ? params.files.map(file => new PassportFile(file, bot)) : undefined;
		this.front_side = params.front_side ? new PassportFile(params.front_side, bot) : undefined;
		this.reverse_side = params.reverse_side ? new PassportFile(params.reverse_side, bot) : undefined;
		this.selfie = params.selfie ? new PassportFile(params.selfie, bot) : undefined;
		this.translation = params.translation ? params.translation.map(translation => new PassportFile(translation, bot)) : undefined;
		this.hash = params.hash;
	}
}

class PassportFile {
	constructor(params = {}, bot) {
		this.file_id = params.file_id;
		this.file_unique_id = params.file_unique_id;
		this.file_size = params.file_size;
		this.file_date = params.file_date;
	}
}

class Dice {
	constructor (params = {}, bot) {
		this.emoji = params.emoji;
		this.value = params.value;
	}
}

class BotCommand {
	constructor (params = {}, bot) {
		this.command = params.command;
		this.description = params.description;
	}
}

/**
 * Available types
 * 
 * All types used in the Bot API responses are represented as JSON-objects, but LibTelegram represented it as classes
 */

module.exports = {
	BotCommand,
	Dice,
	ChatPermissions,
	ChatPhoto,
	InlineKeyboardMarkup,
	InlineKeyboardButton,
	CallbackGame,
	LoginUrl,
	MessageEntity,
	Audio,
	PhotoSize,
	Document,
	Animation,
	Game,
	Sticker,
	MaskPosition,
	Video,
	Voice,
	VideoNote,
	Contact,
	Location,
	Venue,
	PollOption,
	Invoice,
	SuccessfulPayment,
	OrderInfo,
	ShippingAddress,
	PassportData,
	EncryptedCredentials,
	EncryptedPassportElement,
	PassportFile,
	Message,
	InlineQuery,
	ShippingQuery,
	Poll,
	PollAnswer,
	CallbackQuery,
	ChosenInlineResult,
	PreCheckoutQuery,
	User,
	Chat
};